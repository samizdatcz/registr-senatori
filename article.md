---
title: "ANKETA: „Budvar je výjimečný a prospěšný.“ Senátoři se chystají na ‚okleštěný‘ registr smluv"
perex: "Novela zákona o registru smluv, která v Poslanecké sněmovně prošla o jeden hlas, brzy zamíří do Senátu. Horní komora Parlamentu rozhodne, zdali výjimka z registru bude platit pro národní podnik Budvar a státní podniky a společnosti s většinovou účastí státu, krajů nebo obcí. Zpravodajský web Českého rozhlasu proto oslovil všech 81 senátorů. „Budvar by měl požívat mimořádný status vzhledem k jeho výjimečnosti,“ zní například jedna z odpovědí."
description: "Zpravodajský web Českého rozhlasu proto oslovil všech 81 senátorů se dvěma otázkami. Jak budou o klíčovém zákonu hlasovat?"
authors: ["Hana Mazancová"]
published: "13. března 2017"
socialimg: https://interaktivni.rozhlas.cz/registr-senatori/media/anketa-screen.png
url: "registr-senatori"
libraries: [jquery]
styles: ["styl/anketa.css", "styl/magnific-popup.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/registr-poslanci/
    title: ANKETA: Proč 80 poslanců „okleštilo“ registr smluv? Je to složité, obhajují novelu zákonodárci
    perex: Nejprve stačil jeden hlas, aby neprošla. Poté zase o jeden hlas prošla. Málokterá novela zákona měla tak pozoruhodný vývoj jako ta k registru smluv. Zpravodajský web Českého rozhlasu proto oslovil všech osmdesát poslanců, kteří schválili výjimku ze zveřejňování smluv pro národní pivovar Budvar a státní firmy. Odpovědělo 23 z nich.
    image: http://media.rozhlas.cz/_obrazek/3782564--registr-smluv--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1702893
    title: Budvar je zaklínadlem v tažení proti zveřejňování smluv, zní z Rekonstrukce státu
    perex: Poslanci nejsou schopni vysvětlit, proč hlasovali pro plošnou výjimku pro státní podniky k zákonu o registru smluv.
    image: http://media.rozhlas.cz/_obrazek/3810759--josef-karlicky-z-projektu-rekonstrukce-statu--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1701341
    title: Přehledně: kastrace, facka lidem i důvody pro nehlasování. Co řekli politici o výjimce v registru?
    perex: Vykastrování zákona, pokračování plýtvání veřejnými prostředky, potřeba odborného dohledu v dozorčích radách. To jsou některé reakce na odhlasování výjimky ze zákona o registru smluv.
    image: http://media.rozhlas.cz/_obrazek/3783421--poslanecka-snemovna--1-950x0p0.jpeg
  - link: http://www.rozhlas.cz/zpravy/domaci/_zprava/1701687
    title: Jak prošlo osekání registru smluv? Pomohla i klička s &bdquo;vypnutými&ldquo; poslanci z ČSSD
    perex: K osekání klíčové protikorupční iniciativy přispěla i trojice poslanců z ČSSD, která před opakovaným závěrečným hlasováním odešla ze sálu.
    image: http://media.rozhlas.cz/_obrazek/3634971--poslancka-snemovna-2452016-jan-hamacek-a-vojtech-filip--1-950x0p0.jpeg    
---

„Vykostěný“ registr smluv, jak o novele zákona hovoří například jeho zpravodaj Jan Farský (TOP 09), by měl na program schůze Senátu přijít v polovině dubna. V Poslanecké sněmovně přitom schvalování novely zákona mělo zajímavý vývoj.

Pro připomenutí: při prvním hlasování verze s výjimkami z registru neprošla, a to těsně - o jeden hlas. V ten moment se ozval šéf poslaneckého klubu ČSSD Roman Sklenák. Hlasoval prý pro přijetí novely, ale záznamové zařízení mu hlásí, že se hlasování zdržel. Poslanci jeho výtku uznali a hlasovali znovu, přičemž v tom krátkém čase se do hlasování nepřihlásilo celkem pět poslanců. Mimo jiné proto už v druhé kole novela prošla. O jeden hlas. 
 
Zpravodajský web Českého rozhlasu už dříve oslovil všech osmdesát poslanců, kteří pro novelu zákona o registru smluv zvedli ruku. [Všech 23 došlých odpovědí si můžete přečíst ZDE](https://interaktivni.rozhlas.cz/registr-poslanci/).

Nyní redakce oslovila všech 81 senátorů, odpověď do ankety poslalo 47 z nich. Otázky byly následující:

- Mají národní podnik Budvar a společnosti s většinovým podílem státu, krajů a obcí mít výjimku ze zveřejňování v registru smluv? Pokud ano/ne, z jakého důvodu?

- Jak budete o schválené novele hlasovat v Senátu? Budete navrhovat její úpravu? Pokud ano, jakou?

Například podle Lumíra Aschenbrennera z ODS by zveřejňování smluv městské i státní podniky „v hospodářské soutěži výrazně handicapovalo“.
 
„Konečný dopad zhoršeného postavení takového městského podniku by v důsledku mohl vést i ke zdražení služeb pro občany města,“ myslí si senátor. Podobně i jeho stranický kolega Tomáš Grulich souhlasí s výjimkou pro všechny národní podniky. Ty prý už nyní mají vlastní kontrolní mechanismy, a proto registru smluv netřeba. 
 
## „Zavání to diskriminací“
 
Senátorka Zuzana Baudyšová, zvolená na kandidátce hnutí ANO, je naopak pro co nejmenší počet výjimek. „Nejlépe žádné. Takto navržená novela vytváří z registru velmi málo transparentní nástroj pro veřejnou kontrolu jednotlivých organizací státu i samospráv,“ odpověděla v anketě. Podobně partajní kolega Jiří Dušek je pro původní verzi zákona.
 
„Nadšeným podporovatelem“ zákona o registru smluv na počátku nebyl senátor Zdeněk Brož, nestraník zvolený za KDU-ČSL. Když už má ale platit, měl by se podle jeho slov vztahovat na všechny.
 
„Aby z celého zákona zbyly nakonec jako povinné subjekty jen obce a kraje, nedává vůbec žádný smysl,“ uvedl s tím, že je ochoten uvažovat o výjimce pro Budvar. Podporu původní verzi vyjádřil v anketě i jeho stranický kolega Jiří Carbol.
 
To Michael Canov za Starosty pro Liberecký kraj má jasno. „Nemají, není důvod, ochrana obchodního tajemství je ve stávajícím znění zákona zajištěna,“ odpověděl výstižně na dotaz, zdali mají národní podnik Budvar státní firmy a ty s většinovým podílem státu, krajů, nebo obcí mít výjimku z registru smluv. Výjimka pro Budvar podle jeho mínění „zavání diskriminací“. 
 
Naopak Jan Horník za Starosty a nezávislé je přesvědčen, že Budvar by měl požívat „jakýsi mimořádný status“. „Vzhledem k jeho výjimečnosti a celonárodní ekonomické prospěšnosti,“ odpověděl v anketě. Naprosto opačný názor zastává senátor Zbyněk Linhart, zvolený také za Starosty a nezávislé. „Zveřejňovat smlouvy mají buď všichni, nebo nikdo.“
 
Zajímavé bude nicméně hlasování senátorů za sociální demokracii, která má v horní komoře Parlamentu většinu. „Konečná verze sněmovního návrhu nevystihuje smysl původní verze, a proto se po diskusi v rámci senátního klubu připojím ke změnám v Senátu,“ napsala například Milada Emmerová za ČSSD. Podporu schválené novele naopak vyjádřil Jan Hajda nebo Božena Sekaninová. 
 
Někteří z nich však ještě nejsou rozhodnuti, jak se k novele zákona při hlasování postaví.
 
„Již schválení novely o jeden hlas ve sněmovně naznačuje, že problematika rozšíření výjimek ze zákona o registru smluv je a bude ještě velmi problematické a horké téma,“ myslí si například Ivo Bárek (ČSSD).

<aside class="big">
  <div id="anketa">
   <h2 data-bso=1 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">1. Mají národní podnik Budvar a společnosti s většinovým podílem státu, krajů a obcí mít výjimku ze zveřejňování v registru smluv? Pokud ano/ne, z jakého důvodu?</h2>
   <h2 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">2. Jak budete o schválené novele hlasovat v Senátu? Budete navrhovat její úpravu? Pokud ano, jakou?</h2>
   <h3 style="margin-left: 25px; margin-right: 25px; margin-bottom: 20px; margin-top: 30px; color: #606060">Kliknutím na respondenta zobrazíte jeho celé odpovědi</h3>
  </div>
</aside>
